FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>

ARG VERSION_ID
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk add --update curl php7 php7-pcntl php7-fpm php7-xml php7-pgsql php7-pdo_pgsql php7-mbstring \
    php7-fileinfo php7-curl php7-json php7-opcache php7-session php7-gd php7-posix php7-dom gzip && \
    mkdir -p /var/www/ && \
    curl -L https://git.tt-rss.org/fox/tt-rss/archive/${VERSION_ID}.tar.gz \
    | tar -xzC /var/www/ && mv /var/www/tt-rss /var/www/html && \
    chmod -R 777 /var/www/html/lock /var/www/html/cache /var/www/html/feed-icons && \
    sed -i 's/127.0.0.1/0.0.0.0/g' /etc/php7/php-fpm.d/www.conf && \
	{ \
		echo '[global]'; \
		echo 'include=/etc/php7/php-fpm.d/*.conf'; \
	} | tee /etc/php7/php-fpm.conf && \
	{ \
		echo '[global]'; \
		echo 'error_log = /proc/self/fd/2'; \
		echo; \
		echo '[www]'; \
		echo '; if we send this to /proc/self/fd/1, it never appears'; \
		echo 'access.log = /proc/self/fd/2'; \
		echo; \
		echo 'clear_env = no'; \
		echo; \
		echo '; Ensure worker stdout and stderr are sent to the main error log.'; \
		echo 'catch_workers_output = yes'; \
	} | tee /etc/php7/php-fpm.d/docker.conf && \
	{ \
		echo '[global]'; \
		echo 'daemonize = no'; \
		echo; \
		echo '[www]'; \
		echo 'listen = 9000'; \
    } | tee /etc/php7/php-fpm.d/zz-docker.conf && \
    { \
		echo 'opcache.memory_consumption=16'; \
		echo 'opcache.interned_strings_buffer=2'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	}  | tee /etc/php7/conf.d/opcache-recommended.ini
EXPOSE 9000/tcp
USER nobody
VOLUME ["/var/www/html"]
CMD ["php-fpm7"]
